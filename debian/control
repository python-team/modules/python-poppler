Source: python-poppler
Section: python
Priority: optional
Maintainer: Andrea Gasparini <gaspa@yattaweb.it>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 10), dh-python,
 python-all-dev (>=  2.6.6-3~), python-all-dbg, python-gtk2-dev (>= 2.10),
 python-gobject-2-dev (>= 2.10.1), python-cairo-dev (>= 1.15.4), libpoppler-glib-dev (>= 0.15),
 libatk1.0-dev (>= 1.6.0),
 libxcb-render-util0-dev (>= 0.2+git36-1~) | libxcb-render0-dev
Standards-Version: 4.1.1
Vcs-Git: https://salsa.debian.org/python-team/packages/python-poppler.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-poppler
Homepage: https://launchpad.net/poppler-python

Package: python-poppler
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, python-gtk2 (>=2.10.0), python-gobject-2 (>= 2.10.1), python-cairo (>= 1.15.4)
Provides: ${python:Provides}
Description: Poppler Python bindings
 This package includes Python bindings for LibPoppler.
 It is needed to run programs written in Python and using Poppler set.
 LibPoppler is a PDF rendering library based on xpdf PDF viewer, and used by
 kpdf and evince PDF viewers.

Package: python-poppler-dbg
Section: debug
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}, python-poppler (= ${binary:Version}), python-dbg, python-gobject-2-dbg, python-cairo-dbg, python-gtk2-dbg
Provides: ${python:Provides}
Description: Poppler Python bindings (debug extension)
 This package includes Python bindings for LibPoppler.
 It is needed to run programs written in Python and using Poppler set.
 LibPoppler is a PDF rendering library based on xpdf PDF viewer, and used by
 kpdf and evince PDF viewers.
 .
 This package contains the extension built for the Python debug interpreter.
